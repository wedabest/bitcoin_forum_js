import passport from 'passport';
const GoogleStrategy = require('passport-google-oauth20').Strategy;
import {APIUser} from '../models/userAPI';

module.exports = function(passport) {
  passport.serializeUser((user, done) => {
    done(null, user.id);
  });

  passport.deserializeUser((id, done) => {
    APIUser.findById(id).then(user => {
      done(null, user);
    });
  });

  passport.use(
    new GoogleStrategy(
      {
        clientID: process.env.googleClientID,
        clientSecret: process.env.googleClientSecret,
        callbackURL: process.env.googleCallbackURL,
      },
      async (token, refreshtoken, profile, done) => {
        const existingUser = await APIUser.findOne({'google.id': profile.id});

        if(existingUser) {
          return done(null, existingUser);
        } else {
            let user = await new APIUser();
            user.google.id = profile.id;
            user.google.token = token;
            user.google.email = profile.emails[0].value;
            user.google.name = profile.displayName;
            user.save().then(() => {
              done(null, user);
            }).catch((e) => {
              console.log(e);
            });
        };
      }
    )
  );
};
