import {ObjectID} from 'mongodb';
import _ from 'lodash';

//convert month number to strings
const monthDigToStr = (monthDig, year) => {
  let month;
  switch(monthDig) {
    case 0:
      month = 'Jan';
      break;
    case 1:
      month = 'Feb';
      break;
    case 2:
      month = 'Mar';
      break;
    case 3:
      month = 'Apr';
      break;
    case 4:
      month = 'May';
      break;
    case 5:
      month = 'Jun';
      break;
    case 6:
      month = 'Jul';
      break;
    case 7:
      month = 'Aug';
      break;
    case 8:
      month = 'Sep';
      break;
    case 9:
      month = 'Oct';
      break;
    case 10:
      month = 'Nov';
      break;
    case 11:
      month = 'Dec';
      break;
    default: 
      month = `Some time in ${year}`;
  };
  return month;
};

//funciton to create new user object
const userObj = data => {
  const {_id, userId, name, email, abilities, reviews} = data;
  const time = ObjectID(_id).getTimestamp();
  const year = time.getFullYear();
  const month = monthDigToStr(time.getMonth(), year);

  let teacherDetail = new Object();
  teacherDetail.id = userId;
  teacherDetail.name = name;
  teacherDetail.email = email;
  teacherDetail.year = year;
  teacherDetail.month = month;
  teacherDetail.abilities = abilities;
  teacherDetail.reviews = reviews;
  
  return teacherDetail
};

//function to go through teachers data
const returnTeacherDetail = userData => {
  try {
    if(userData !== undefined && userData!== null && userData !== "" && userData instanceof Object) {
      if(userData.constructor !== Array) {
        return userObj(userData)
      } else {
        if(userData.length !== 0){
          return userData.map(teacher => {
            //create a new teacher object each time
            return userObj(teacher)
          });
        } else {
          throw 'Data Not Found';
        }
      }
    } else {
      throw 'Unexpected Error';
    }
  }
  catch(e) {
    return e;
  }
};

export default returnTeacherDetail;