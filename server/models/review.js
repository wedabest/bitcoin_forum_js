import mongoose from 'mongoose';
import {User} from './user';

const Schema = mongoose.Schema;

let Review = new Schema({
  title: {
    type: String,
    required: true,
    minlength: 1,
    trim: true
  },
  text: {
    type: String,
    required: true,
    minlength: 1,
    maxlength: 50
  },
  _creator: {
    type: Schema.Types.ObjectId,
    required: true
  },
  teacher: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  }
});

Review = mongoose.model('Review', Review);
module.exports = {Review};
