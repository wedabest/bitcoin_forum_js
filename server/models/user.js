import mongoose from 'mongoose';
import validator from 'validator';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs'

import Review from './review';

const Schema = mongoose.Schema;

let User = new Schema({
  userId: {
    type: String,
    required: true
  },
  email:{
    type: String,
    required: true,
    trim: true,
    minlength: 1,
    unique: true,
    validate:[
      {
        validator: function validateEmail(email) {
          return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
        },
        msg: '{VALUE} is not a valid email'
      }
    ]
  },
  name:{
    type: String,
    minlength: 3,
    maxlength: 15,
    validate:[
      {
        validator: function(str) {
          return !/[~`!#$%\^&*+=\-\[\]\\';,/{}|\\":<>\?]/g.test(str);
        },
        msg: "{VALUE} is not a name"
      }
    ]
  },
  password:{
    type: String,
    minlength: 6,
    required: true
  },
  abilities: [{
    type: String
  }],
  teacher: {
    type: Boolean,
    default: false,
    required: true
  },
  reviews: [{
    type: Schema.Types.ObjectId,
    ref: 'Review'
  }],
  tokens: [{
    access:{
      type: String,
      required: true
    },
    token: {
      type: String,
      required: true
    }
  }]
});

//Document Method Generate Auth Token
User.methods.generateAuthToken = function() {
  let user = this;
  let access = 'auth';
  let token = jwt.sign({
    _id: user._id.toHexString(),
    access,
    expiresIn: process.env.EXP_TIME
  }, process.env.JWT_SECRET).toString();

  user.tokens.push({access, token});

  return user.save().then(() => {
    return token;
  });
};

//Document Method Remove Token
User.methods.removeToken = function(token) {
  let user = this;

  return user.update({
    $pull: {
      tokens:{token}
    }
  });
};

//Model Method Find By Token, Used In Authentication
User.statics.findByToken = function(token) {
  let User = this;
  let decodedToken;

  try {
    decodedToken = jwt.verify(token, process.env.JWT_SECRET);
  } catch(e) {
    return Promise.reject()
  }

  return User.findOne({
    '_id': decodedToken._id,
    'tokens.token': token,
    'tokens.access': 'auth'
  });
};

//Model Method Find By Credentials, Used in Login
User.statics.findByCredentials = function(email, password) {
  let User = this;

  return User.findOne({email})
    .then(user => {
      if(!user) {
        return Promise.reject('User not found.');
      } else {
        return new Promise((resolve, reject) => {
          bcrypt.compare(password, user.password)
            .then(res => {
              if(res) {
                resolve(user);
              } else {
                reject('Wrong password.');
              }
            });
        });
      }
    })
    .catch(e => Promise.reject(e));
};

//Schema.pre Serial
User.pre('save', function (next) {
  let user = this;

  if(user.isModified('password')) {
    bcrypt.genSalt(10, function(err, salt) {
      bcrypt.hash(user.password, salt, function(err, hash) {
        user.password = hash;
        next();
      });
    });
  } else {
      next();
  };
});

User = mongoose.model('User', User);
module.exports = {User}
