import mongoose from 'mongoose';

const Schema = mongoose.Schema;

let APIUser = new Schema({
  google: {
    id: {
      type: String,
      unique: true
    },
    token: {
      type: String
    },
    name: {
      type: String
    },
    email: {
      type: String
    }
  }
});

APIUser = mongoose.model('APIUser', APIUser);
module.exports = {APIUser};
