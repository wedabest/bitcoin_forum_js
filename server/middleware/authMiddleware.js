import {User} from '../models/user';

//Authentication
const authenticate = (req, res, next) => {
  let token = req.header('x-auth');

  User.findByToken(token).then(user => {
    if(!user) {
      return Promise.reject();
    }

    req.user = user;
    req.token = token;
    next();
    return null;
  }).catch((e) => {
    res.status(401).send(e);
  });
};

module.exports = {authenticate};
