import mongoose from 'mongoose';
import bluebird from 'bluebird';

mongoose.Promise = bluebird;

mongoose.connect(
  process.env.MONGODB_URI,
  {
    useMongoClient: true
  }).then(() => {
    console.log('connected to db');
  }).catch((e) => {
    console.log(`connected not ${e}`);
  });

module.exports = mongoose;
