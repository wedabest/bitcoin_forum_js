import express from 'express';
import bodyParser from 'body-parser';
import http from 'http';
import cookieParser from 'cookie-parser';

import config from './config/config'; //this has to be imported for the app settings
import routes from './routes';

let app = express();
app.server = http.createServer(app);

//middleware
app.use(bodyParser.json({
  limit: process.env.BODY_LIMIT
}));

app.use(cookieParser());

app.use('*', (req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

app.use('/api', routes); //all routes stored in route.js

const port = process.env.PORT;

app.server.listen(port, () => {
  console.log(`Magic start at port ${app.server.address().port}`);
});

export default app;
