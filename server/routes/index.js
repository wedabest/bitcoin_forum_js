import express from 'express';
import session from 'express-session';
import passport from 'passport';

import initialiseDB from '../db/mongoose';
import User from '../controller/user';
import APIUser from '../controller/userAPI';
import Teacher from '../controller/teacher';

require('../service/passport')(passport);

let app = express();

app.use(session(
  {
    secret:process.env.COOKIE_SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: {
      maxAge: 1000000000
    }
  })
);

app.use(passport.initialize());
app.use(passport.session());

app.use('/user', User);
app.use('/apiuser', APIUser);
app.use('/main', Teacher);

export default app;
