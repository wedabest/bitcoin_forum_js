import {Router} from 'express';
import _ from 'lodash';
import uuidv1 from 'uuid';

import {User} from '../models/user';
import {authenticate} from '../middleware/authMiddleware';

const router = Router();

//Register
router.post('/register', (req, res) => {
  let body = _.pick(req.body, ['email', 'name', 'password', 'abilities', 'teacher']);
  body.userId = uuidv1();

  let user = new User(body);

  user.save()
    .then(() => {
      return user.generateAuthToken();
    })
    .then(token => {
      res.status(200).header('x-auth', token).send(user);
    })
    .catch(e => {
      res.status(400).send(e);
    });
});

//Login
router.post('/login', (req, res) => {
  let body = _.pick(req.body, ['email', 'password']);

  User.findByCredentials(body.email, body.password)
    .then(user => { user.generateAuthToken()
      .then(token => { 
        res.status(200).header('x-auth', token).send(token) 
      });
    })
    .catch(e => {
      if(e === 'Wrong password.') {
        res.status(401).send(e);
      } else if(e === 'User not found.') {
        res.status(404).send(e);
      } else {
        res.status(500).send(e);
      }
    });
});

//After Login or register
router.get('/me', authenticate, (req, res) => {
  res.status(200).send(req.user);
});

//Logout
router.delete('/logout', authenticate, (req, res) => {
  req.user.removeToken(req.token).then(() => {
    res.status(200).send('logged out');
  }).catch(e => {
    res.status(400).send('error');
  });
});

//get teachers by name
router.get('/:name', (req, res) => {
  let name = req.params.name;

  User.find({
    name
  }).then(users => {
    res.send(returnTeacherDetail(users));
  }).catch(e => {
    res.status(400).send(e);
  });
});

module.exports = router;
