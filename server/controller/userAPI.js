import express from 'express';
import passport from 'passport';

let router = express.Router();

//Register
router.get('/auth/google',
  passport.authenticate
  (
    'google',
    {
      scope : [
        'profile',
        'email'
      ]
  })
);

//Callback Return from Google Site
router.get('/auth/google/callback', passport.authenticate('google'), (req, res) => {
    res.redirect('./profile');
  }
);

//Logout
router.get('/logout', (req, res) => {
  req.logout();
  res.redirect('/');
})

//Current User
router.get('/current_user', (req, res) => {
  res.send(req.user);
});

module.exports = router;
