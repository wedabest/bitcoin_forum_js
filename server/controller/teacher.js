import {Router} from 'express';
import {ObjectID} from 'mongodb';
import _ from 'lodash';

import {User} from '../models/user';
import {Abilities} from '../models/abilities';
import {Review} from '../models/review';
import {authenticate} from '../middleware/authMiddleware';
import returnTeacherDetail from '../service/userObjUtil';

let router = Router();

const responseHandler = (result, res) => {
  if(result.length !== 0 && result !== 'Data Not Found') {
    res.status(200).send(result);
  } else if (result === 'Unexpected Error') {
    res.status(500).send(result)
  } else {
    res.status(404).send(result)
  }
};

//get all abilities 
router.get('/abilities', (req, res) => {
  Abilities.find({})
    .then(skills => {
    res.send(skills);
  }).catch(e => {
    res.status(400).send(e);
  });
});

//get all teacher
router.get('/teachers', (req, res) => {
  User.find({
    teacher: true
  }).then(teachers => {
    responseHandler(returnTeacherDetail(teachers), res);
  }).catch(e => {
    res.status(400).send(e);
  });
});

//get one teacher by id 
router.get('/teacher/:id', (req, res) => {
  let id = req.params.id;

  if(!ObjectID.isValid(id)) {
    return res.status(404).send('Invalid ID');
  }

  User.findById(id)
    .then(teacher => {
      responseHandler(returnTeacherDetail(teacher), res);
  }).catch(e => {
    res.status(400).send(e);
  });
});

//get teachers by name
router.get('/teachers/:name', (req, res) => {
  let name = req.params.name;

  User.find({
    teacher: true,
    name
  }).then(teachers => {
    res.send(returnTeacherDetail(teachers));
  }).catch(e => {
    res.status(400).send(e);
  });
});

//post review for a teacher
router.post('/teacher/review/add/:id', authenticate, (req, res) => {
  let id = req.params.id;

  if(!ObjectID.isValid(id)) {
    return res.status(404).send('Invalid ID');
  }

  User.findById(id).then(teacher => {
    let review = new Review({
      title: req.body.title,
      text: req.body.text,
      _creator: req.user._id,
      teacher: teacher._id
    });

    review.save().then(doc => {
      teacher.reviews.push(doc)
      teacher.save().then(() => {
        res.status(200).send();
      }).catch(e => {
        res.status(400).send(e);
      });
    })
  })
});

//update review
router.patch('/reviews/:id', authenticate, (req, res) => {
  const id = req.params.id;
  let text = _.pick(req.body, ['text']);

  if (!ObjectID.isValid(id)) {
    return res.status(404).send('Invalid ID');
  }

  Review.findOneAndUpdate({
    _id: id,
    _creator: req.user._id
  }, {
    $set: text
  }, {
    new: true
  }).then(review => {
    if(!review) {
      res.status(404).send();
    }
    res.status(200).send();
  }).catch(e => {
    res.status(400).send(e);
  })
})

//delete review
router.delete('/review/remove/:id', authenticate, (req, res) => {
  const id = req.params.id;

  if (!ObjectID.isValid(id)) {
    return res.status(404).send('Invalid ID');
  }

  Review.findOne({
    _id: id,
    _creator: req.user._id
  }).then(review => {
    if(!review) {
      res.status(400).send();
    }
    res.status(200).send(review);
  }).catch(e => {
    res.status(400).send(e);
  });
});

module.exports = router;
